from django.db import models

# Create your models here.

class Customer(models.Model):
    nama = models.CharField(max_length=255, default='')
    phone = models.CharField(max_length=13, default='')

class Product(models.Model):
    nama = models.CharField(max_length=255, default='')
    harga = models.IntegerField(default=0)
    CUSTOMERS = (
        ('Umum', 'Umum'),
        ('Member', 'Member'),
    )
    customer_price = models.CharField(max_length=50, choices=CUSTOMERS, default='Umum')

class Membership(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True)

class Order(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.SET_NULL, null=True)
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True)
    bayar = models.IntegerField(default=0)
    status_pelanggan = models.CharField(max_length=50, blank=True, null=True)

    def save(self, *args, **kwargs):
        if self.product:
            self.status_pelanggan = self.product.customer_price
        super().save(*args, **kwargs)