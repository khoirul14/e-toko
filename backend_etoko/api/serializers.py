from rest_framework import serializers
from . import models

class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Customer
        fields = '__all__'

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Product
        fields = '__all__'

class OrderSerializer(serializers.ModelSerializer):
    customer_detail = CustomerSerializer(source='customer', many=False, read_only=True)
    product_detail = ProductSerializer(source='product', many=False, read_only=True)
    class Meta:
        model = models.Order
        fields = '__all__'

class MembershipSerializer(serializers.ModelSerializer):
    customer_detail = CustomerSerializer(source='customer', many=False, read_only=True)
    class Meta:
        model = models.Membership
        fields = '__all__'