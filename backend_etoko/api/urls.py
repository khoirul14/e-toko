from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views

router = DefaultRouter()
router.register('customer', views.CustomerViewSet)
router.register('product', views.ProductViewSet)
router.register('order', views.OrderViewSet)
router.register('membership', views.MembershipViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('product/<kategori>', views.ProductForCustomerFilter.as_view({'get': 'list'}), name='product-for-cutomer-filter'),
    path('order/<status_pelanggan>', views.OrderCustomerFilter.as_view({'get': 'list'}), name='order-status-pelanggan-filter'),
]
