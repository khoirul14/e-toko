from django.shortcuts import render
from rest_framework import viewsets
from . import models
from . import serializers

# Create your views here.


class CustomerViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.CustomerSerializer
    queryset = models.Customer.objects.all()

class OrderViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.OrderSerializer
    queryset = models.Order.objects.all()

class ProductViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.ProductSerializer
    queryset = models.Product.objects.all()

class MembershipViewSet(viewsets.ModelViewSet):
    serializer_class = serializers.MembershipSerializer
    queryset = models.Membership.objects.all()

class ProductForCustomerFilter(viewsets.ModelViewSet):
    serializer_class = serializers.ProductSerializer
    # queryset = models.Product.objects.all()

    def get_queryset(self):
        kategori = self.kwargs['kategori']
        return models.Product.objects.filter(customer_price__iexact=kategori)

class OrderCustomerFilter(viewsets.ModelViewSet):
    serializer_class = serializers.OrderSerializer

    def get_queryset(self):
        status_pelanggan = self.kwargs['status_pelanggan']
        return models.Order.objects.filter(status_pelanggan__iexact=status_pelanggan)